use std::error::Error;
use std::fs;
use std::i16;
use std::str::FromStr;

type MyResult = Result<(), Box<dyn Error>>;

// x, y, number of squares in area
struct Location(i16, i16, u32);

struct Point
{
	x: i16,
	y: i16
}

impl Location
{
	fn new(x: i16, y: i16) -> Self
	{
		Self(x, y, 0)
	}

	fn calc_distance(&self, ox: i16, oy: i16) -> i16
	{

		(ox - self.0).abs() + (oy - self.1).abs()
	}
}

impl FromStr for Coordinate {
    type Err = Box<Error>;

    fn from_str(s: &str) -> Result<Coordinate> {
        let comma = match s.find(",") {
            None => return Err(From::from("could not find comma")),
            Some(i) => i,
        };
        let (pos1, pos2) = (&s[..comma].trim(), s[comma + 1..].trim());
        Ok(Coordinate { x: pos1.parse()?, y: pos2.parse()? })
    }
}

fn main() -> MyResult
{
	let input = fs::read_to_string("../day6.txt")?;
	let mut locs: Vec<Location> = Vec::with_capacity(50);

	for line in input.lines()
	{
		let temp: Vec<&str> = line.split(", ").collect(); // optimise?
		let x: i16 = temp[0].parse().unwrap();
		let y: i16 = temp[1].parse().unwrap();
		locs.push(Location::new(x,y));
	}

	// part1(&mut locs)?;
	test(&mut locs)?;

	Ok(())
}

// Guesses: 267 - low
//		   5000 - high
fn part1(locs: &mut Vec<Location>)
{
	let mut max_x = 0i16;
	let mut max_y = 0i16;
	let mut min_x = i16::MAX;
	let mut min_y = i16::MAX;

	for l in locs.iter()
	{
		if l.0 > max_x { max_x = l.0 }
		if l.1 > max_y { max_y = l.1 }
		if l.0 < min_x { min_x = l.0 }
		if l.1 < min_y { min_y = l.1 }
	}

	let mut dists: Vec<i16> = Vec::new();
	for y in min_y..=max_y
	{
		for x in min_x..=max_x
		{
			for i in locs.iter()
			{
				dists.push(i.calc_distance(x, y));
			}
			let mut min = (0, dists[0]);
			for i in 1..dists.len()
			{
				if dists[i] < min.1
				{
					min = (i, dists[i]);
				}
			}
			locs[min.0].2 += 1;
			dists.clear();
		}
	}

	locs.sort_by(|a, b| a.2.cmp(&b.2));
	for x in locs
	{
		println!("{}", x.2);
	}
}


fn test(locs: &mut Vec<Location>)
{
	let mut max_x = 0i16;
	let mut max_y = 0i16;
	let mut min_x = i16::MAX;
	let mut min_y = i16::MAX;

	for l in locs.iter()
	{
		if l.0 > max_x { max_x = l.0 }
		if l.1 > max_y { max_y = l.1 }
		if l.0 < min_x { min_x = l.0 }
		if l.1 < min_y { min_y = l.1 }
	}

	let mut seen: Vec<i16> = Vec::new();
	for y in min_y..=max_y
	{
		for x in min_x..=max_x
		{
			let mut best: (usize, i16) = (51, i16::MAX);
			for (i, val) in locs.iter().enumerate()
			{
				let temp = val.calc_distance(x, y);
				if temp == best.1
				{
					break; // CHECK THIS!!!
				}
				if temp < best.1
				{
					best = (i, temp);
				}
			}
			locs[best.0].2 += 1;
		}
	}

	locs.sort_by(|a, b| a.2.cmp(&b.2));
	for x in locs
	{
		println!("{}", x.2);
	}
}
