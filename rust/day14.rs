const ITERATIONS: usize = 880751;

fn main()
{
	part1();
	part2();
}

fn part1()
{
	let mut recipes: Vec<u8> = Vec::with_capacity(1_800_000);
	recipes.push(3);
	recipes.push(7);

	let mut elf1: usize = 0;
	let mut elf2: usize = 1;

	let mut s1;
	let mut s2;
	let mut len;
	loop
	{
		s1 = recipes[elf1];
		s2 = recipes[elf2];
		bake(&mut recipes, s1, s2);
		len = recipes.len();
		elf1 = (((s1 + 1) as usize + elf1) as usize) % len;
		elf2 = (((s2 + 1) as usize + elf2) as usize) % len;
		if len > ITERATIONS + 10
		{
			break;
		}
	}

	for i in ITERATIONS..ITERATIONS+10
	{
		print!("{}", recipes[i]);
	}
	println!();
}

fn part2()
{
	let pattern = &[8,8,0,7,5,1];

	let mut recipes: Vec<u8> = Vec::with_capacity(20_400_000);
	recipes.push(3);
	recipes.push(7);

	let mut elf1: usize = 0;
	let mut elf2: usize = 1;

	let mut s1;
	let mut s2;
	let mut len;
	loop
	{
		s1 = recipes[elf1];
		s2 = recipes[elf2];
		bake(&mut recipes, s1, s2);
		len = recipes.len();
		elf1 = (((s1 + 1) as usize + elf1) as usize) % len;
		elf2 = (((s2 + 1) as usize + elf2) as usize) % len;
		
		if recipes.ends_with(pattern)
		{
			println!("{}", len - 6);
			break;
		}
		if recipes[..len-1].ends_with(pattern)
		{
			println!("{}", len - 7);
			break;
		}
	}
}

fn bake(input: &mut Vec<u8>, score1: u8, score2: u8)
{
	let mut sum: u8 = score1 + score2;
	let digit2 = sum % 10;
	sum /= 10;

	if sum != 0
	{
		input.push(sum);
	}
	input.push(digit2);
}
