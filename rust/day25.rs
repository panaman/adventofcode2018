use std::error::Error;
use std::fs;

type MyResult = Result<(), Box<Error>>;

fn main() -> MyResult
{
	let input = fs::read_to_string("test.txt")?;
	let mut sky: Vec<Star> = Vec::new();

	for line in input.lines()
	{
		let temp: Vec<i8> = line.split(',')
								.map(|x| x.parse().unwrap())
								.collect();
		sky.push(Star::new(temp[0], temp[1], temp[2], temp[3]));
	}

	part1(&mut sky)?;
	Ok(())
}

#[derive(Debug)]
struct Star
{
	x : i8,
	y : i8,
	z : i8,
	w : i8
}

#[derive(Debug)]
struct Constellation(Vec<Star>);

impl Constellation
{
	fn new() -> Self
	{
		Self(Vec::new())
	}

	fn is_connected(self, other: Constellation) -> bool
	{
		for x in self.0.iter()
		{
			for y in other.0.iter()
			{
				if x.distance(y) <= 3
				{
					return true;
				}
			}
		}
		false
	}
}

impl Star
{
	fn new(a: i8, b: i8, c: i8, d: i8) -> Self
	{
		Self {x: a, y: b, z: c, w: d}
	}

	fn distance(&self, other: &Star) -> i8
	{
		(other.x - self.x).abs()
		+ (other.y - self.y).abs()
		+ (other.z - self.z).abs()
		+ (other.w - self.w).abs()
	}
}

fn part1(sky: &mut Vec<Star>) -> MyResult
{
	// let mut groups : Vec<Vec<Star>> = Vec::new();
	let mut constellation: Vec<Star> = Vec::new();
	let mut temp = sky.pop().unwrap();

	// constellation.push(temp);
	// while !sky.is_empty()
	// {

	// }

	// constellation.push(temp);
	// for x in 0..sky.len()
	// {
	// 	if temp.distance(sky[x])
	// }

	Ok(())
}