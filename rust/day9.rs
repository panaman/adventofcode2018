use std::collections::VecDeque;

const PLAYERS: usize = 465;

fn main()
{
	println!("{}", play(71_940));
	println!("{}", play(7_194_000));
}

fn play(max_marble: usize) -> usize
{
    let mut board = VecDeque::with_capacity(max_marble);
    let mut scores = vec![0; PLAYERS];

    board.push_front(0);
    for marble in 1..=max_marble
    {
        // through the loop, keep current position at front
        let player = (marble - 1) % PLAYERS;
        if marble % 23 == 0
        {
            // Rotate board right 7 times
            board.rotate_right(7);
            scores[player] += marble + board.pop_front().unwrap();
        }
        else
        {
            // Move forward two times and insert the marble into the board
            board.rotate_left(2);
            board.push_front(marble);
        }
    }
    *scores.iter().max().unwrap()
}
