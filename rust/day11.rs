use std::error::Error;
use std::fs;

type MyResult = Result<(), Box<Error>>;

fn main() -> MyResult
{
	let input = fs::read_to_string("day11.txt")?;

	let words: Vec<&str> = input.lines().collect::<Vec<&str>>();

	part1(&words);
	part2(&words);

	Ok(())
}