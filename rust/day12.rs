use std::collections::HashMap;
use std::error::Error;
use std::fs;

type MyResult = Result<(), Box<Error>>;

fn main() -> MyResult
{
	let mut pots = "##..#..##.#....##.#..#.#.
					##.#.#.######..##.#.#.###
					#.#..#...##...#....#....#
					.##.###..#..###...#...#..".to_vec();

	pots.reserve(100);

	println!("{:?}", pots.get(0));

	Ok(())
}