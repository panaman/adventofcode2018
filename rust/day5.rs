use std::error::Error;
use std::fs;
use std::usize;

type MyResult = Result<(), Box<dyn Error>>;
type Polymer = Vec<Atom>;

#[derive(Clone, Debug)]
struct Atom
{
	data: u8,
	react: bool
}

impl Atom
{
	fn new(x: u8) -> Self
	{
		Self {data: x, react: false}
	}

	fn mark(&mut self)
	{
		self.react = true;
	}
}

impl From<u8> for Atom
{
	fn from(x: u8) -> Self
	{
		Atom::new(x)
	}
}

fn main() -> MyResult
{
	let input = fs::read_to_string("../day5.txt")?;
	let mut poly: Polymer = input.bytes().map(|x| Atom::from(x)).collect();

	part1(&mut poly)?;
	part2(&mut poly)?;

	Ok(())
}

fn part1(input: &mut Polymer) -> MyResult
{
	println!("{:?}", react_poly(input));
	// assert!(input.len() == 11720);
	Ok(())
}

fn part2(input: &mut Polymer) -> MyResult
{
	let mut smallest: usize = usize::MAX;
	let mut current: usize;
	for letter in b'a'..=b'z' {
		let mut temp = input.clone();
		temp.retain(|x| !x.data.eq_ignore_ascii_case(&letter));

		current = react_poly(&mut temp);
		if current < smallest {
			smallest = current;
		}
	}
	println!("{:?}", smallest);
	// assert!(input.len() == 4956);
	Ok(())
}

fn react_poly(input: &mut Polymer) -> usize
{
	let mut reacted = false;
	let mut i = 0;
	loop {
		while i < input.len() - 1 {
			if input[i].data ^ input[i+1].data == 32 {
				reacted = true;
				input[i].mark();
				input[i+1].mark();
				i += 1;
			}
			i += 1;
		}
		i = 0;
		if reacted {
			input.retain(|x| !x.react);
			reacted = false;
			continue;
		}
		return input.len();
	}
}
