use std::collections::HashMap;
use std::error::Error;
use std::fs;

type MyResult = Result<(), Box<dyn Error>>;

fn main() -> MyResult
{
	let input = fs::read_to_string("../day2.txt")?;

	let words: Vec<&str> = input.lines().collect::<Vec<&str>>();

	part1(&words);
	part2(&words);

	Ok(())
}

fn part1(input: &Vec<&str>)
{
	let mut seen: HashMap<char, u16> = HashMap::new();
	let mut two: u32 = 0;
	let mut three: u32 = 0;

	for word in input
	{
		for x in word.chars()
		{
			if !seen.contains_key(&x)
			{
				seen.insert(x, 1);
			}
			else
			{
				if let Some(x) = seen.get_mut(&x)
				{
					*x += 1;
				}
			}
		}

		let mut foundtwo = false;
		let mut foundthree = false;
		for (_, val) in seen.iter()
		{
			if !foundtwo && *val == 2
			{
				two += 1;
				foundtwo = true;
				continue;
			}
			else if !foundthree && *val == 3
			{
				three += 1;
				foundthree = true;
				continue;
			}
		}
		seen.clear();
	}
	println!("{}", two * three);
}

fn part2(input: &Vec<&str>)
{
	let len = input.len();
	let mut word = String::new();
	for x in 0..len
	{
		for y in x+1..len
		{
			word = common_letters(input[x], input[y]);
			if word.len() == 25
			{
				println!("{:?}", word);
				return;
			}
		}
	}
	println!("None found");
}

fn common_letters(first: &str, second: &str) -> String
{
	let mut diff_seen = false;
	let mut common: String = String::new();
	for (c1, c2) in first.chars().zip(second.chars())
	{
		if c1 == c2
		{
			common.push(c1);
		}
	}

	common
}	
