use std::collections::HashSet;
use std::error::Error;
use std::fs;

type MyResult = Result<(), Box<dyn Error>>;

fn main() -> MyResult
{
    let input = fs::read_to_string("../day1.txt")?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> MyResult
{
    let mut num: i64 = 0;

    for line in input.lines()
    {
        num += line.parse::<i64>()?;
    }
    println!("{}", num);

    Ok(())
}

fn part2(input: &str) -> MyResult
{
    let mut num = 0i64;
    let mut seen: HashSet<i64> = HashSet::new();
    seen.insert(0);

    'outer:
    loop
    {
        for line in input.lines()
        {
            num += line.parse::<i64>()?;
            if !seen.contains(&num)
            {
                seen.insert(num);
            }
            else
            {
                break 'outer;
            }
        }
    }
    println!("{}", num);

    Ok(())
}
