#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define NUM_LINES 1004

void part2(int*, int);

int main()
{
	FILE* input = fopen("day1.txt", "r");
	int* nums = malloc(NUM_LINES * sizeof(int));

	int total = 0;
	int cur;
	int i = 0;
	while (fscanf(input, "%d\n", &cur) != EOF) {
		total += cur;
		nums[i++] = cur;
	}
	printf("part1: %d\n", total);
	fclose(input);

	part2(nums, i);
	free(nums);
	return 0;
}

void part2(int* in, int len)
{
	int pos = 0;
	int* seen = malloc(len * 200 * sizeof(int));
	if (seen == NULL)
		return;
	int total = 0;
	while (1) {
		total += in[pos % len];
		for (int i = 0; i < pos; i++) {
			if (seen[i] == total) {
				printf("part2: %d\n", total);
				goto exit;
			}
		}
		seen[pos++] = total;
	}
exit:
	free(seen);
}

