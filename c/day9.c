#include <stdio.h>
#include <stdlib.h>

//static const int players = 418;
//static const int maxMarble = 70769;
static const int players = 9;
static const int maxMarble = 4;

struct node {
	struct node* next;
	int val;
};

struct deque {
	struct node* front;
	struct node* back;
};

int pop_front(struct deque*);
int pop_back(struct deque*);
void push_front(struct deque*, int);
void push_back(struct deque*, int);

int main()
{
	unsigned int* const scores = calloc(players, sizeof(unsigned int));

	struct node* head = malloc(sizeof(struct node));
	head->val = 0;
	head->next = NULL;

	struct deque* dq = malloc(sizeof(struct deque));
	dq->front = head;
	dq->back = head;

	// start the game
	int player = 0;
	int marble = 1;
	for (; marble <= maxMarble; marble++) {
		if (marble % 23 == 0) {
			scores[player] += marble;
			goto next_player;
		}
		for (int k = 0; k < 2; k++) {
			int val = pop_front(dq);
			//push_back(dq, val);
		}
		push_back(dq, marble);
	next_player:
		player = (player + 1) % players;
	}

	struct node* temp = dq->front;
	while (temp != NULL) {
		printf("%d\n", temp->val);
		temp = temp->next;
	}
	putchar('\n');

	// freeing marbles
	struct node* curr = dq->front;
	struct node* next;
	while (curr != NULL) {
		next = curr->next;
		free(curr);
		curr = next;
	}
	free(scores);
	free(dq);
	return 0;
}

int
pop_front(struct deque* dq)
{
/*
	struct node* temp = dq->front;
	int val = temp->val;
	dq->front = temp->next;
	//FIXME: fix this
	if (dq->front == NULL)
		dq->back = NULL;
	else
		dq->front->prev = NULL;

	free(temp);
	return val;
*/
	return 0;
}

int
pop_back(struct deque* dq)
{
	return 0;
}

//FIXME: prev/next assignment incorrect
void
push_front(struct deque* dq, int marble)
{
	struct node* new = malloc(sizeof(struct node));
	new->val = marble;
	new->next = dq->front;

	dq->front = new;
}

//FIXME: prev/next assignment incorrect
void
push_back(struct deque* dq, int marble)
{
	struct node* new = malloc(sizeof(struct node));
	new->val = marble;
	new->next = NULL;

	dq->back = new;
	//printf("%d %d\n", new->prev->val, new->val);
}

