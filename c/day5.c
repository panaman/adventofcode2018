#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

// test2.txt answer is 11720 part1, 4956 part2
int main()
{
	FILE* input = fopen("test2.txt", "r");
	fseek(input, 0L, SEEK_END);
	long len = ftell(input) - 1;
	rewind(input);

	char* polymer = malloc(len);
	char* reacted = malloc(len);
	fscanf(input, "%s", polymer);

	size_t pos;
	bool didReact;
	do {
		pos = 0;
		didReact = false;
		memset(reacted, 0, len);
		while (polymer[pos] != 0) {
			// 'a' ^ 'A' = 32
			if ((polymer[pos] ^ polymer[pos + 1]) == 32) {
				didReact = true;
				pos += 2;
				continue;
			}
			if ((polymer[pos + 1] ^ polymer[pos + 2]) == 32) {
				didReact = true;
				strncat(reacted, &polymer[pos], 1);
				pos += 3;
				continue;
			}
			strncat(reacted, &polymer[pos], 3);
			pos += 3;
		}

		char* temp = polymer;
		polymer = reacted;
		reacted = temp;
		printf("%s\n", polymer);
		printf("Length: %ld\n", strlen(polymer));
	} while (didReact);

	//printf("%s\n", reacted);
	//printf("Length: %ld\n", strlen(reacted));

	free(polymer);
	free(reacted);
	return 0;
}

