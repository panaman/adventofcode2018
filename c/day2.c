#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#define WORD_SIZE 26

void part2(FILE* input);

int main()
{
	FILE* input = fopen("day2.txt", "r");

	int dubAmt = 0;
	int triAmt = 0;

	char* word = malloc(WORD_SIZE + 1);
	//char* words[WORD_SIZE + 1];
	bool foundDub = false;
	bool foundTri = false;
	while (fscanf(input, "%s", word) != EOF) {
		foundDub = false;
		foundTri = false;

		char counts[26] = {0};
		int i = 0;
		for (; i < WORD_SIZE; i++) {
			counts[word[i] - 97]++;
		}
		for (i = 0; i < 26; i++) {
			if (counts[i] == 2 && !foundDub) {
				foundDub = true;
				dubAmt++;
			}
			else if (counts[i] == 3 && !foundTri) {
				foundTri = true;
				triAmt++;
			}
		}
	}
	printf("part1: %d\n", dubAmt * triAmt);
	free(word);

	return 0;
}

